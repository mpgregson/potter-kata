interface BookCollection {
  id: number;
  books: Book[];
}
