import { blockStatement } from "@babel/types";

const BOOK_PRICE = 8;

const calculateCollectionTotal = (numberOfBooks: number): number => {
  switch (numberOfBooks) {
    case 1:
      return BOOK_PRICE;
    case 2:
      return numberOfBooks * BOOK_PRICE * 0.95;
    case 3:
      return numberOfBooks * BOOK_PRICE * 0.9;
    case 4:
      return numberOfBooks * BOOK_PRICE * 0.8;
    case 5:
      return numberOfBooks * BOOK_PRICE * 0.75;
    default:
      return BOOK_PRICE;
  }
};

const getCheapestQualifyingCollection = (
  basket: BookCollection[],
  bookId: number
): BookCollection | null => {
  let cheapestTotal = Number.MAX_VALUE;
  let cheapestCollection: BookCollection | null = null;

  basket.map(bookCollection => {
    if (bookCollection.books.find(book => book.id === bookId)) return null;

    const collectionTotal =
      calculateCollectionTotal(bookCollection.books.length + 1) +
      basket
        .filter(c => c.id !== bookCollection.id)
        .reduce((acc, cur) => {
          return acc + calculateCollectionTotal(cur.books.length);
        }, 0);

    if (collectionTotal < cheapestTotal) {
      cheapestTotal = collectionTotal;
      cheapestCollection = bookCollection;
    }
  });
  return cheapestCollection;
};

const calculateTotal = (books: Book[]): number => {
  let counter = 0;
  const bookCollections = books.reduce(
    (basket: BookCollection[], book: Book): BookCollection[] => {
      const cheapestCollection = getCheapestQualifyingCollection(
        basket,
        book.id
      );
      if (cheapestCollection) {
        const newCollection = {
          id: cheapestCollection.id,
          books: [...cheapestCollection.books, book]
        };

        return [
          ...basket.filter(b => b.id !== cheapestCollection.id),
          newCollection
        ];
      }
      counter++;
      return [...basket, { id: counter, books: [book] }];
    },
    []
  );

  return bookCollections.reduce((totalPrice, bookCollection) => {
    return totalPrice + calculateCollectionTotal(bookCollection.books.length);
  }, 0);
};

export { calculateTotal };
