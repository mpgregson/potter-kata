import { calculateTotal } from "./index";

test.each`
  bookIds                     | expectedTotal
  ${[]}                       | ${0}
  ${[1]}                      | ${8}
  ${[1, 1]}                   | ${16}
  ${[1, 2]}                   | ${15.2}
  ${[1, 2, 3]}                | ${21.6}
  ${[1, 2, 3, 4]}             | ${25.6}
  ${[1, 2, 3, 4, 5]}          | ${30}
  ${[1, 2, 3, 1]}             | ${29.6}
  ${[1, 1, 2, 2, 3, 3, 4, 5]} | ${51.2}
  ${[1, 2, 3, 2, 3, 3, 1]}    | ${51.2}
`("return the cheapest price", ({ bookIds, expectedTotal }) => {
  const books: Book[] = bookIds.map((bid: number) => {
    return {
      id: bid
    };
  });

  const price = calculateTotal(books);
  expect(price).toEqual(expectedTotal);
});
